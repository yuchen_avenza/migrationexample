## Core Data Migration Unit Testing

#### Heavyweight Core Data Migration 

I did some reading and experiments for core data migration and unit testing this week. There are two types of core data migration: *lightweight migration* and *heavyweight migration*. Core data is able to infer the mapping from a old schema to the new one for some simple schema changes ([doc](https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/CoreDataVersioning/Articles/vmLightweightMigration.html#//apple_ref/doc/uid/TP40004399-CH4-SW2)), such as:

* Add or remove a attributes
* A non-optional attribute becoming optional and vice versa
* Renaming an entity, attribute, or a relationship

If core date can infer this mapping, it can perform the migration automatically for us. This is *lightweight migration*. 

However, when the schema changes are complicated, then we will need to generate the mapping ourselves by creating a *core data mapping model* and implmented the [`NSEntityMigrationPolicy`](https://developer.apple.com/library/mac/documentation/Cocoa/Reference/NSEntityMigrationPolicy_class/index.html). Basically, this migration policy will allow us to have access to both versions of *managed object context*, and thus allow us to do any customization to the *entires* and their *relationships*. This is *heavyweight migration*, which is a bit more complecated but a lot more powerful. 

#### Unit Testing

Without unit testing, the developing process of migration is somwhat like the following:

1. Run the app and generate some data.  
2. Make some changes to the app (including data model update) and relaunch the app again. Old verson of the data should get migrated on the first launch of the new version of the app. 

We need to constantly go back and forth between *step 1* and *step 2*. For example, we may start with migrating a "Placemark". After that, we can be working on migrating a "Placemark with custom icon". After everthing with that is working, we need to double check with and make sure migrating a "Placemark" and without custom icon is still working since we have changed the code. 

The benifits of using Unit Testing is that if we break any previous logics when adding new code, we will notice it right away through some regression testing. And also, as we are developing, we don't need to constantly checkout the previous version of the app and generate test data manually. 

#### Should we add Unit Testing to our current iOS 2.5? 

The answer is I don't know. 

It relativly easy for me to create a simple unit test such as "check whether the title of this placemark is altered after migration or not". But this, doesn't sound like too benifitial to our current state of the app. If we cannot cover as much as we can in unit testing, it is nothing more than a piece of example code. 

If we do want to cover more, based on the complexity of the current core data model, it maybe time consuming. And also, the current migrate code is not very well written for unit testing. Threrefore, I am not sure how much need to be changed in order to make it suitable for unit testing and whether or not these changes will introduce new bugs. 

But no matter what, I created a [sample code](https://bitbucket.org/yuchen_avenza/migrationexample/) with heavweight migration and unit testing for the reference of whatever our next step will be. 
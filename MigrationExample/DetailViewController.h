//
//  DetailViewController.h
//  MigrationExample
//
//  Created by Yuchen on 2015-03-03.
//  Copyright (c) 2015 Yuchen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end


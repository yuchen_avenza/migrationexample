//
//  MigrationExampleTests.m
//  MigrationExampleTests
//
//  Created by Yuchen on 2015-03-03.
//  Copyright (c) 2015 Yuchen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import <CoreData/CoreData.h>

// Reference: http://chanson.livejournal.com/115621.html

// Reference: Blog By Grahma Lee: Unit Testing Core Data-driven Apps
// http://iamleeg.blogspot.ca/2009/09/unit-testing-core-data-driven-apps.html

// Reference: Blog by Apple Engineer talking about including Unit Testing in Core Data
// Tag:
// http://www.friday.com/bbum/2005/09/24/unit-testing/

// Reference: A complete tutorial about data base migration and unit testing
// Tag: progress
// http://www.objc.io/issue-4/core-data-migration.html


// Goal:
// 1) in-memory test store type

@interface MigrationExampleTests : XCTestCase

@property (nonatomic, strong) NSManagedObjectModel *model1;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator1;
@property (nonatomic, strong) NSManagedObjectContext *context1;
@property (nonatomic, strong) NSPersistentStore *store1;

@property (nonatomic, strong) NSManagedObjectModel *model2;

@end

@implementation MigrationExampleTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    {
        // Load previous version of schemas http://stackoverflow.com/questions/11168770/
        NSString *resourceURL = [NSString stringWithFormat:@"MigrationExample.momd/MigrationExample"];
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:resourceURL withExtension:@"mom"];
        _model1 = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
        _coordinator1 = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model1];
        
        NSURL *url1 = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"temp1.sqlite"]];
        [[NSFileManager defaultManager] removeItemAtURL:url1 error:nil];
        _store1 = [_coordinator1 addPersistentStoreWithType:NSSQLiteStoreType
                                              configuration:nil
                                                        URL:url1
                                                    options:nil
                                                      error:nil];
        
        _context1 = [[NSManagedObjectContext alloc] init];
        [_context1 setPersistentStoreCoordinator:_coordinator1];
    }
    
    {
        NSString *resourceURL = [NSString stringWithFormat:@"MigrationExample.momd/MigrationExample 2"];
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:resourceURL withExtension:@"mom"];
        _model2 = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
    
    //
    // In the old context, create a entity Event
    //
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event"
                                              inManagedObjectContext:self.context1];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name]
                                                                      inManagedObjectContext:self.context1];
    
    [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
    
    // Save the context.
    NSError *error = nil;
    if (![self.context1 save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate.
        // You should not use this function in a shipping application, although
        // it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    // Note: You don’t need a mapping model if you’re able to use lightweight migration
    NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:self.model1
                                                                          destinationModel:self.model2];
    
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:@[[NSBundle mainBundle]]
                                                            forSourceModel:self.model1
                                                          destinationModel:self.model2];
    
    if (mappingModel == nil) {
        // deal with the error
        XCTAssertTrue(mappingModel!=nil, @"Could not find the mapping model. ");
    }
    
    NSURL *url1 = [self.coordinator1 URLForPersistentStore:self.store1];
    NSURL *url2 = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.sqlite"]];
    [[NSFileManager defaultManager] removeItemAtURL:url2 error:nil];
    
    BOOL migrationSuccess = [migrationManager migrateStoreFromURL:url1
                                                             type:NSSQLiteStoreType
                                                          options:nil
                                                 withMappingModel:mappingModel
                                                 toDestinationURL:url2
                                                  destinationType:NSSQLiteStoreType
                                               destinationOptions:nil
                                                            error:nil];
    XCTAssertTrue(migrationSuccess, @"Migration is failed. ");
    
    NSPersistentStoreCoordinator* coordinator2 = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model2];
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![coordinator2 addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url2 options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    NSManagedObjectContext *context2 = [[NSManagedObjectContext alloc] init];
    [context2 setPersistentStoreCoordinator:coordinator2];
    
    
    
    NSFetchRequest *request1 = [[NSFetchRequest alloc] init];
    [request1 setEntity:[NSEntityDescription entityForName:@"Event"
                                    inManagedObjectContext:self.context1]];
    NSUInteger count = [self.context1 countForFetchRequest:request1 error:&error];
    if (count==NSNotFound) {
        NSLog(@"[Unit Testing] Migration is not sucessfull. ");
    } else {
        NSLog(@"[Unit Testing] Number of EventString entities that's migrated: %ld", count);
    }
    
    // For validation, after the migration is done. Do a search for Entity "EventString"
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"EventString"
                                   inManagedObjectContext:context2]];
    NSUInteger count1 = [context2 countForFetchRequest:request error:&error];
    if (count1==NSNotFound) {
        NSLog(@"[Unit Testing] Migration is not sucessfull. ");
    } else {
        NSLog(@"[Unit Testing] Number of EventString entities that's migrated: %ld", count1);
    }
    
    XCTAssertEqual(count, count1);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
